#include <stdio.h>
#define N 256
int main()
{
    int str[N]={0};
    int i, x;
    puts("Enter a string, please:");
    while((x=getch())!='\r')
    {
        putchar(x);
        str[x]++;
    }
    puts("");
    for(i=0;i<N;i++)
    {
        if (str[i]>0)
            printf("%c-%d\n", i, str[i]);
    }
    return 0;
}
